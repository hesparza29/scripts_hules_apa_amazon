<?php
  ini_set('max_execution_time', 1200);
  require_once("./funciones/conexionBBDD_AWS.php");
  $baseAWS = conexionBBDD_AWS();
  $nombreArchivo = fopen("./../archivos_de_carga/productos_inactivos.csv", "r") or die("Problemas al abrir el archivo");
  $contador = 0;
    while (!feof($nombreArchivo)){
            $linea = fgets($nombreArchivo);
            $linea = trim($linea);

            //Medimos el tamaño de cada linea porque en la última linea nos dara 0 y dará un error al tratar de separar la cadena
            if(strlen($linea)>0){
              $linea = explode(",", $linea);
              if($linea[0]!="numero_apa"){
                  
                  $consultaActivaProducto = "UPDATE products SET status_product_id=? WHERE apa_id=?";
                  $resultadoActivaProducto = $baseAWS->prepare($consultaActivaProducto);
                  $resultadoActivaProducto->execute(array(2, $linea[0]));
                  if($resultadoActivaProducto->rowCount()==1){
                      $contador++;
                  }
                  $resultadoActivaProducto->closeCursor();
              }
            }

    }

    fclose($nombreArchivo);

    echo "Se activaron un total de " . $contador . " productos " ;
    $baseAWS = null;

?>