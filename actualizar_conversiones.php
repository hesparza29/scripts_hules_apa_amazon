<?php
  ini_set('max_execution_time', 1200);
  require_once("./funciones/conexionBBDD_AWS_Develop.php");
  require_once("./funciones/conexionBBDD_AWS.php");
  $baseAWS = conexionBBDD_AWS();
  $nombreArchivo = fopen("./../archivos_de_carga/conversiones.csv", "r") or die("Problemas al abrir el archivo");
  $contador = 0;

  //Consulta para actualizar las conversiones de un producto
  $consultaActualizarConversion = "UPDATE products SET original_id=?, dea_id=?, anc_id=?, 
                              oem_id=?, westar_id=?, syd_id=?, grob_id=?, ahm_id=?,
                              monroe_id=?, gates_id=?, tepeyac_id=?, cauplas_id=?, 
                              dayco_id=?, continental_id=?  WHERE apa_id=?";
  $resultadoActualizarConversion = $baseAWS->prepare($consultaActualizarConversion);

  while (!feof($nombreArchivo)){
    $linea = fgets($nombreArchivo);
    $linea = trim($linea);

    //Medimos el tamaño de cada linea porque en la última linea nos dara 0 y dará un error al tratar de separar la cadena
    if(strlen($linea)>0){
    
      $linea = explode(",", $linea);
        (strlen($linea[2])==1) ? $linea[2] = "" : $linea[2] = $linea[2];
        (strlen($linea[3])==1) ? $linea[3] = "" : false;
        (strlen($linea[4])==1) ? $linea[4] = "" : false;
        (strlen($linea[5])==1) ? $linea[5] = "" : false;
        (strlen($linea[6])==1) ? $linea[6] = "" : false;
        (strlen($linea[7])==1) ? $linea[7] = "" : false;
        (strlen($linea[8])==1) ? $linea[8] = "" : false;
        (strlen($linea[9])==1) ? $linea[9] = "" : false;
        (strlen($linea[10])==1) ? $linea[10] = "" : false;
        (strlen($linea[11])==1) ? $linea[11] = "" : false;
        (strlen($linea[12])==1) ? $linea[12] = "" : false;
        (strlen($linea[13])==1) ? $linea[13] = "" : false;
        (strlen($linea[14])==1) ? $linea[14] = "" : false;
        (strlen($linea[15])==1) ? $linea[15] = "" : false;
    
      if($linea[1]!="apa_id"){
        $resultadoActualizarConversion->execute(array($linea[2], $linea[3], $linea[4], $linea[5], $linea[6], 
                                                      $linea[7], $linea[8], $linea[9], $linea[10], $linea[11], 
                                                      $linea[12], $linea[13], $linea[14], $linea[15], $linea[1]));
        if($resultadoActualizarConversion->rowCount()==1){
          $contador++;
        }
        $resultadoActualizarConversion->closeCursor();
      }
    }

  }

  fclose($nombreArchivo);

  $baseAWS = null;

  echo "Se actualizaron las conversiones de un total de " . $contador . " productos " ;

?>