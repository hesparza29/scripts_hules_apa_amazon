<?php 
    ini_set('max_execution_time', 600);
    require_once("./funciones/conexionBBDD_SAE.php");
    require_once("./funciones/conexionBBDD_AWS.php");

    $baseSAE = conexionBBDD_SAE();
    $baseAWS = conexionBBDD_AWS();
    //Seleccionar la clave del vendedor al que se le asignaran los clientes
    $claveVendedor = "TLK1";
    $numeroVendedor = "";
    $numeroCliente = "";
    $contador = 0;
    $flag = 0;

    //Consultar de la base de datos de SAE los clientes que le pertenecen al vendedor seleccionado
    $consultaClientesAsignados = "SELECT CLIE01.CLAVE, CLIE01.NOMBRE, CLIE01.CVE_VEND  FROM CLIE01 
                                    INNER JOIN VEND01 ON 
                                    CLIE01.CVE_VEND=VEND01.CVE_VEND 
                                    WHERE VEND01.NOMBRE=?";
    $resultadoClientesAsignados = $baseSAE->prepare($consultaClientesAsignados);
    $resultadoClientesAsignados->execute(array($claveVendedor));

    while ($registroClientesAsignados = $resultadoClientesAsignados->fetch(PDO::FETCH_ASSOC)){
    
        //Verificar que la consulta del user_id solamente se realice una vez
        if($flag==0){
            //Limpiar la cadena del numero de vendedor devuelta de la base de datos de SAE
            $numeroVendedor = str_replace(" ", "", $registroClientesAsignados["CVE_VEND"]);
            //Obtener el user_id del vendedor
            $consultaUserId = "SELECT user_id FROM sellers WHERE seller_number=?";
            $resultadoUserId = $baseAWS->prepare($consultaUserId);
            $resultadoUserId->execute(array($numeroVendedor));
            if($resultadoUserId->rowCount()==1){
                $registroUserId = $resultadoUserId->fetch(PDO::FETCH_ASSOC);
                $flag++;
            }
            $resultadoUserId->closeCursor();
        }

        //Limpiar la cadena del numero de cliente devuelta de la base de datos de SAE
        $numeroCliente = str_replace(" ", "", $registroClientesAsignados["CLAVE"]);
        //Actualizar el vendedor de los clientes
        $consultaActualizaVendedor = "UPDATE clients SET seller_id=? WHERE client_number=?";
        $resultadoActualizaVendedor = $baseAWS->prepare($consultaActualizaVendedor);
        $resultadoActualizaVendedor->execute(array(intval($registroUserId["user_id"]), $numeroCliente));
        if($resultadoActualizaVendedor->rowCount()==1){
            $contador++;
        }
        $resultadoActualizaVendedor->closeCursor();
        
    }

    $resultadoClientesAsignados->closeCursor();

    echo "Se asignaron un total de " . $contador . " clientes al vendedor " . $claveVendedor . 
            " con user_id " . $registroUserId["user_id"];
    $baseSAE = null;
    $baseAWS = null;
?>