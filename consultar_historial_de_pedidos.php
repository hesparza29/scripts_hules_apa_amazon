<?php

    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_AWS.php");
    require_once("./funciones/fechaPostgre.php");

    $baseAWS = conexionBBDD_AWS();
    $contenido = "Folio,Cliente,Nombre del Cliente,Fecha,Registrado por\n";
    $contador = 0;

    //Consulta para obtener el historial de pedidos
    $consultaPedidos = "SELECT (SELECT client_number FROM public.clients INNER JOIN public.users ON 
                            public.clients.id=public.users.client_id WHERE public.users.id=sale_orders.client_id),
                            (SELECT business_name FROM public.clients INNER JOIN public.users ON 
                            public.clients.id=public.users.client_id WHERE public.users.id=sale_orders.client_id),
                            (SELECT full_name FROM public.users WHERE id=user_id),
                            sale_orders.created_at, folio FROM public.sale_orders INNER JOIN public.users ON  
                            sale_orders.user_id=users.id WHERE sale_orders.created_at BETWEEN ? AND ?
                            ORDER BY sale_orders.id DESC";
    $resultadoPedidos = $baseAWS->prepare($consultaPedidos);
    $resultadoPedidos->execute(array('2022-02-01', '2022-02-28'));
    while($registroPedidos = $resultadoPedidos->fetch(PDO::FETCH_ASSOC)){
        //Añadiendo la información de los pedidos al contenido del archivo
        $contenido .= $registroPedidos["folio"] . ",";
        $contenido .= $registroPedidos["client_number"] .  ",";
        $contenido .= str_replace(",", "",$registroPedidos["business_name"]) . ",";
        $contenido .= fechaPostgre($registroPedidos["created_at"]) . ",";
        $contenido .= $registroPedidos["full_name"] . "\n";
        $contador++;
    }
    $resultadoPedidos->closeCursor();


    $baseAWS = null;

    //Creando el archivo
    $archivo = fopen("../archivos_de_descarga/historial de pedidos.csv", "w");
    fwrite($archivo, $contenido);
    fclose($archivo);

    echo "Hay un total de " . $contador . " pedidos<br />";
?>