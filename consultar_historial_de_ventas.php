<?php

    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_AWS.php");
    require_once("./funciones/fechaPostgre.php");

    $baseAWS = conexionBBDD_AWS();
    $contenido = "Folio,Cliente,Nombre del Cliente,Fecha,Total\n";
    $contador = 0;

    //Consulta para obtener el historial de ventas
    $consultaVentas = "SELECT (SELECT client_number FROM public.clients INNER JOIN public.users ON 
                            public.clients.id=public.users.client_id WHERE public.users.id=sales.client_id),
                            (SELECT business_name FROM public.clients INNER JOIN public.users ON 
                            public.clients.id=public.users.client_id WHERE public.users.id=sales.client_id),
                            total,
                            sales.created_at, folio FROM public.sales INNER JOIN public.users ON  
                            sales.user_id=users.id WHERE sales.created_at BETWEEN ? AND ?
                            ORDER BY sales.id DESC";
    $resultadoVentas = $baseAWS->prepare($consultaVentas);
    $resultadoVentas->execute(array('2021-01-01', '2022-02-28'));
    while($registroVentas = $resultadoVentas->fetch(PDO::FETCH_ASSOC)){
        //Añadiendo la información de los ventas al contenido del archivo
        $contenido .= $registroVentas["folio"] . ",";
        $contenido .= $registroVentas["client_number"] .  ",";
        $contenido .= str_replace(",", "",$registroVentas["business_name"]) . ",";
        $contenido .= fechaPostgre($registroVentas["created_at"]) . ",";
        $contenido .= round($registroVentas["total"]*100)/100 . "\n";
        $contador++;
    }
    $resultadoVentas->closeCursor();


    $baseAWS = null;

    //Creando el archivo
    $archivo = fopen("../archivos_de_descarga/historial de ventas.csv", "w");
    fwrite($archivo, $contenido);
    fclose($archivo);

    echo "Hay un total de " . $contador . " ventas<br />";
?>