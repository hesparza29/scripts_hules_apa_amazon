<?php

    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_AWS.php");
    require_once("./funciones/fechaPostgre.php");

    $baseAWS = conexionBBDD_AWS();
    $contenido = "Número APA,Descripción,Categoría,Versión,Modelo,Línea,Marca,Ubicación,Año Inicio,Año Fin,Especificaciones\n";
    $contador = 0;

    //Consulta para obtener los productos y sus aplicaciones
    $consultaProductos = "SELECT products.apa_id as numero_apa, products.name as descripcion,
                            categories.name as categoria, versions.name as version,
                            models.name as modelo, lines.name as linea, brands.name as marca,
                            locations.name as locacion, year_ini, year_end, aplications.specifications
                            from products inner join relation_product_aplications
                            on products.id=relation_product_aplications.product_id
                            inner join aplications on aplications.id=relation_product_aplications.aplication_id
                            inner join versions on versions.id=aplications.version_id
                            inner join models on models.id=versions.model_id
                            inner join lines on lines.id=models.line_id
                            inner join brands on brands.id=lines.brand_id
                            inner join locations on locations.id=aplications.location_id
                            inner join categories on products.category_id=categories.id 
                             order by products.id";
    $resultadoProductos = $baseAWS->prepare($consultaProductos);
    $resultadoProductos->execute(array());
    while($registroProductos = $resultadoProductos->fetch(PDO::FETCH_ASSOC)){
        $contenido .= $registroProductos["numero_apa"] . ",";
        $contenido .= $registroProductos["descripcion"] . ",";
        $contenido .= $registroProductos["categoria"] . ",";
        $contenido .= $registroProductos["version"] . ",";
        $contenido .= $registroProductos["modelo"] . ",";
        $contenido .= $registroProductos["linea"] . ",";
        $contenido .= $registroProductos["marca"] . ",";
        $contenido .= $registroProductos["locacion"] . ",";
        $contenido .= $registroProductos["year_ini"] . ",";
        $contenido .= $registroProductos["year_end"] . ",";
        $contenido .= $registroProductos["specifications"] . "\n";
        $contador++;
    }
    $resultadoProductos->closeCursor();
    


    $baseAWS = null;

    //Creando el archivo
    $archivo = fopen("../archivos_de_descarga/productos y sus aplicaciones.csv", "w");
    fwrite($archivo, $contenido);
    fclose($archivo);

    echo "Hay un total de " . $contador . " productos<br />";
?>