<?php

    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_AWS.php");

    $baseAWS = conexionBBDD_AWS();
    $contenido = "id Producto,Numero APA,Precio,Estatus\n";
    $contador = 0;

    //Consulta para obtener los productos y sus aplicaciones
    $consultaProductos = "SELECT products.id, products.apa_id, products.price, status_products.name as estatus 
                            FROM products INNER JOIN status_products 
                            ON products.status_product_id=status_products.id order by products.id";
    $resultadoProductos = $baseAWS->prepare($consultaProductos);
    $resultadoProductos->execute(array());
    while($registroProductos = $resultadoProductos->fetch(PDO::FETCH_ASSOC)){
        $contenido .= $registroProductos["id"] . ",";
        $contenido .= $registroProductos["apa_id"] . ",";
        $contenido .= $registroProductos["price"] . ",";
        $contenido .= $registroProductos["estatus"] . "\n";
        $contador++;
    }
    $resultadoProductos->closeCursor();
    


    $baseAWS = null;

    //Creando el archivo
    $archivo = fopen("../archivos_de_descarga/lista de precios.csv", "w");
    fwrite($archivo, $contenido);
    fclose($archivo);

    echo "Hay un total de " . $contador . " productos<br />";
?>