<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <?php
        ini_set('max_execution_time', 600);
        require_once('./funciones/conexionBBDD_AWS.php');
        header("Content-Type:application/xls");
        header("Content-Disposition: attachment; filename=Productos con espacios.xls");

        $baseAWS = conexionBBDD_AWS();
        $contador = 0;

        $consultaProductosConEspacio = "SELECT id, apa_id, name FROM products WHERE apa_id LIKE ?";
        $resultadoProductosConEspacio = $baseAWS->prepare($consultaProductosConEspacio);
        $resultadoProductosConEspacio->execute(array('% %'));
        
    ?>
    <table>
        <tr>
            <th>id</th>
            <th>numero_apa</th>
            <th>descripcion</th>
        </tr>
        <? while ($registroProductosConEspacio = $resultadoProductosConEspacio->fetch(PDO::FETCH_ASSOC)) :?>

                        <tr>
                                <td><?= $registroProductosConEspacio["id"]?></td>
                                <td><?= $registroProductosConEspacio["apa_id"]?></td>
                                <td><?= $registroProductosConEspacio["name"]?></td>
                        </tr>

        <? endwhile?>

    </table>
    <?php
        $resultadoProductosConEspacio->closeCursor();
        $baseAWS = null;
    ?>
  </body>
</html>