<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <?php
        ini_set('max_execution_time', 600);
        require_once('./funciones/conexionBBDD_AWS.php');
        header("Content-Type:application/xls");
        header("Content-Disposition: attachment; filename=Productos con sus conversiones.xls");

        $baseAWS = conexionBBDD_AWS();

        $consultaProductosConConversiones = "SELECT id, apa_id, vazlo_id, star_id, dai_id, original_id 
                                        FROM public.products";
        $resultadoProductosConConversiones = $baseAWS->prepare($consultaProductosConConversiones);
        $resultadoProductosConConversiones->execute(array());

    ?>

    <table>
        <tr>
            <th>id</th>
            <th>numero_apa</th>
            <th>numero_vazlo</th>
            <th>numero_star</th>
            <th>numero_dai</th>
            <th>numero_original</th>
        </tr>
        <? while($registroProductosConConversiones = $resultadoProductosConConversiones->fetch(PDO::FETCH_ASSOC)) :?>

                        <tr>
                                <td><?= $registroProductosConConversiones["id"]?></td>
                                <td><?= $registroProductosConConversiones["apa_id"]?></td>
                                <td><?= $registroProductosConConversiones["vazlo_id"]?></td>
                                <td><?= $registroProductosConConversiones["star_id"]?></td>
                                <td><?= $registroProductosConConversiones["dai_id"]?></td>
                                <td><?= $registroProductosConConversiones["original_id"]?></td>
                        </tr>

        <? endwhile?>

    </table>
    <?php
        $resultadoProductosConConversiones->closeCursor();
        $baseAWS = null;
    ?>
  </body>
</html>