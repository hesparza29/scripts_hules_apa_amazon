<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <?php
        ini_set('max_execution_time', 600);
        require_once('./funciones/conexionBBDD_AWS.php');
        header("Content-Type:application/xls");
        header("Content-Disposition: attachment; filename=Productos Inactivos.xls");

        $baseAWS = conexionBBDD_AWS();

        $consultaProductosInactivos = "SELECT products.apa_id, products.name, status_products.name as estatus, products.price
                                        FROM products INNER JOIN status_products 
                                        ON products.status_product_id=status_products.id";
        $resultadoProductosInactivos = $baseAWS->prepare($consultaProductosInactivos);
        $resultadoProductosInactivos->execute(array());

    ?>

    <table>
        <tr>
            <th>numero_apa</th>
            <th>descripcion</th>
            <th>estatus</th>
        </tr>
        <? while($registroProductosInactivos = $resultadoProductosInactivos->fetch(PDO::FETCH_ASSOC)) :?>

                        <tr>
                                <td><?= $registroProductosInactivos["apa_id"]?></td>
                                <td><?= $registroProductosInactivos["name"]?></td>
                                <td><?= $registroProductosInactivos["estatus"]?></td>
				<td><?= $registroProductosInactivos["price"]?></td>
                        </tr>

        <? endwhile?>

    </table>
    <?php
        $resultadoProductosInactivos->closeCursor();
        $baseAWS = null;
    ?>
  </body>
</html>