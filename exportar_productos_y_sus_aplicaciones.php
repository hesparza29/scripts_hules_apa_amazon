<?php
    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_AWS.php");
    $baseAWS = conexionBBDD_AWS();
    $numeroAPA = array();
    $contador = 0;
    $contenido = "APA,Descripción,Categoría,Marca,Modelo,Línea,Version,Años,Ubicación,Especificación\n";
    $nombreArchivo = fopen("../archivos_de_carga/productos_y_sus_aplicaciones.csv", "r") or die("Problemas al abrir el archivo");
  
    while (!feof($nombreArchivo)){
            $linea = fgets($nombreArchivo);
            $linea = trim($linea);
  
            //Medimos el tamaño de cada linea porque en la última linea nos dara 0 y dará un error al tratar de separar la cadena
            if(strlen($linea)>0){
                $linea = explode(",", $linea);
                if($linea[0]!="numero_apa"){
                    $numeroAPA[$contador] = $linea[0];
                    $contador++;
                }
            }
    }
  
    fclose($nombreArchivo);
    
    $consultaAplicaciones = "SELECT products.apa_id as numero_apa, products.name as descripcion,
					categories.name as categoria, versions.name as version,
                    models.name as modelo, lines.name as linea, brands.name as marca,
                    locations.name as locacion, year_ini, year_end, aplications.specifications
                    from products inner join relation_product_aplications
                    on products.id=relation_product_aplications.product_id
                    inner join aplications on aplications.id=relation_product_aplications.aplication_id
                    inner join versions on versions.id=aplications.version_id
                    inner join models on models.id=versions.model_id
                    inner join lines on lines.id=models.line_id
                    inner join brands on brands.id=lines.brand_id
                    inner join locations on locations.id=aplications.location_id
        			inner join categories on products.category_id=categories.id
      				where products.apa_id=?";
    $resultadoAplicaciones = $baseAWS->prepare($consultaAplicaciones);

    for($i=0; $i < $contador; $i++){
        $resultadoAplicaciones->execute(array($numeroAPA[$i]));
        $registroAplicaciones = $resultadoAplicaciones->fetch(PDO::FETCH_ASSOC);
        $contenido .= $numeroAPA[$i] . "," . $registroAplicaciones["descripcion"] . "," . 
                        $registroAplicaciones["categoria"] . "," . $registroAplicaciones["version"] . "," . 
                        $registroAplicaciones["modelo"] . "," . $registroAplicaciones["linea"] . "," . 
                        $registroAplicaciones["marca"] . "," . $registroAplicaciones["locacion"] . "," . 
                        $registroAplicaciones["year_ini"] . "," . $registroAplicaciones["year_end"] . "," . 
                        $registroAplicaciones["specifications"] . "\n";
    }
    
    $baseAWS = null;

    //Creando el archivo
    $archivo = fopen("../archivos_de_descarga/Producto APA y sus aplicaciones.csv", "w");
    fwrite($archivo, $contenido);
    fclose($archivo);

    echo "Hay un total de " . $contador . " productos<br />";
?>