<?php

  //Función que me permite hacer la conexión a la base de datos de Local(Sistema gestor)
  function conexionBBDD_Local(){
    $base = new PDO("mysql:host=localhost;dbname=aplicacion","root","");
    $base->exec("set names utf8");
    $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $base;
  }
?>
