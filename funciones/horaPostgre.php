<?php

  function horaPostgre($timestamp){
    $horaConsulta = explode(" ", $timestamp);
    $horaConsulta = $horaConsulta[1];
    $horaConsulta = explode(".", $horaConsulta);
    $horaConsulta = $horaConsulta[0];
    $horaConsulta = explode(":", $horaConsulta);
    $horaEstandar = ($horaConsulta[0]-6) . ":" . $horaConsulta[1] . ":" . $horaConsulta[2];

    return $horaEstandar;
  }
?>
