<?php

  function timepoTranscurrido($fechaEstatusAnterior, $fechaEstatusActual){
    //Obtener la hora en el estatus anterior
    $tiempoAnterior = explode(".", $fechaEstatusAnterior);
    $tiempoAnterior = $tiempoAnterior[0];

    //Obtener la hora en el estatus actual
    $tiempoActual = explode(".", $fechaEstatusActual);
    $tiempoActual = $tiempoActual[0];
    // $tiempoActual = $tiempoActual[0];
    // //Crear el objeto tipo Date del tiempo actual
    // $miTiempo = new DateTime($tiempoActual);
    //
    // $miTiempo->modify('-' . $tiempoAnterior[2] . ' second');
    // $miTiempo->modify('-' . $tiempoAnterior[1] . ' minute');
    // $miTiempo->modify('-' . $tiempoAnterior[0] . ' hours');
    //
    // return $miTiempo;

    $fecha2 = new DateTime($tiempoAnterior);//fecha inicial
    $fecha1 = new DateTime($tiempoActual);//fecha de cierre

    $intervalo = $fecha1->diff($fecha2);

    return $intervalo;
  }

?>
