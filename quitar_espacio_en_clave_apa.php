<?php

    ini_set('max_execution_time', 1200);
    require_once("./funciones/conexionBBDD_AWS.php");

    $baseAWS = conexionBBDD_AWS();
    $numeroApa = "";
    $contador = 0;

    $consultaProductosConEspacio = "SELECT id, apa_id FROM products WHERE apa_id LIKE ?";
    $resultadoProductosConEspacio = $baseAWS->prepare($consultaProductosConEspacio);
    $resultadoProductosConEspacio->execute(array('% %'));

    while ($registroProductosConEspacio = $resultadoProductosConEspacio->fetch(PDO::FETCH_ASSOC)){
        //Quitar el espacio a los productos y actualizar el numero_apa
        $numeroApa = str_replace(" ", "", $registroProductosConEspacio["apa_id"]);
        
        $consultaActualizaNumeroApa = "UPDATE products SET apa_id=? WHERE id=?";
        $resultadoActualizaNumeroApa = $baseAWS->prepare($consultaActualizaNumeroApa);
        $resultadoActualizaNumeroApa->execute(array($numeroApa, $registroProductosConEspacio["id"]));
        if($resultadoActualizaNumeroApa->rowCount()==1){
            $contador++;
        }
        $resultadoActualizaNumeroApa->closeCursor();
    }

    $resultadoProductosConEspacio->closeCursor();

    echo "En total se actualizaron " . $contador . " productos";

    $baseAWS = null;

?>